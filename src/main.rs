extern crate irc;
extern crate pest;
#[macro_use]
extern crate pest_derive;

use std::env;
use std::thread;
use std::time;

use irc::client::prelude::*;

use pest::Parser;

#[derive(Parser)]
#[grammar = "sentence.pest"]
struct SentenceParser;

fn main() {
    let cfgfile = env::args().nth(1).expect("Please give a config file");
    let server = IrcClient::new(cfgfile).expect("Failed to connect to server");
    server.identify().expect("Failed to identify");
    println!("Identified");
    server.for_each_incoming(|message| {
        let requestor_nick = match message.source_nickname() {
            Some(s) => s.to_owned(),
            None => String::new()
        };
        match message.command {
            Command::PRIVMSG(channel, message) => {
                if let Ok(parsed_str) = SentenceParser::parse(Rule::phrase, &message) {
                    let parsed_str = parsed_str.clone().next().unwrap().into_inner();
                    for parsed in parsed_str {
                        let nick = extract_nick(&parsed, &requestor_nick);

                        match parsed.as_rule() {
                            Rule::list => server.send_privmsg(&channel, "zb list").unwrap(),
                            Rule::source => server.send_privmsg(&channel, "https://gitlab.com/waylon531/zeebbot").unwrap(),
                            Rule::help => server.send_privmsg(&channel,
                            "Zello! My name is Zeebbot. I am native to Zeebonia, but I zave come to Zamerica to learn your beautiful languaz. Zeebonia is my native languaz, zo I would be very zappy to zelp you to tranzlate Zeebonian to Zenglish.").unwrap(),
                            Rule::add => {
                                let number = extract_number(&parsed);
                                server.send_privmsg(&channel, &format!("zb add {} as {}",number,&nick)).unwrap();
                            },
                            Rule::sub => {
                                let number = extract_number(&parsed);
                                server.send_privmsg(&channel, &format!("zb add -{} as {}",number,&nick)).unwrap();
                            },
                            Rule::buy => {
                                let item = extract_item(&parsed);
                                server.send_privmsg(&channel, &format!("zb buy {} as {}",item,&nick)).unwrap();
                            },
                            Rule::repeat_buy => {
                                let number = extract_number(&parsed) as usize;
                                //Skip the number, then get the item
                                let item = extract_item_inner(&mut parsed.clone().into_inner().skip(1).next().unwrap());
                                for _ in 0 .. number {
                                    server.send_privmsg(&channel, &format!("zb buy {} as {}",item,&nick)).unwrap();
                                    thread::sleep(time::Duration::from_secs(1));
                                }
                            },
							e @ _ => {println!("{:?}",e)}
                        }
                    }
                }

            }
            _ => ()
        }
    }).unwrap()

}
fn extract_number<'a>(parsed: &pest::iterators::Pair<'a,Rule>) -> f64 {
    let mut digits = String::new();
    //println!("Extracting number: {:?}",parsed);
    let number = parsed.clone().into_inner().next();
    //match number.map(|x| x.as_rule()) {
    if let Some(number) = number {
        match number.as_rule() {
            Rule::number => {
                for digit in number.into_inner() {
                    //println!("{:?}",&digit.clone());
                    match digit.as_rule() {
                        Rule::zero => digits.push('0'),
                        Rule::one => digits.push('1'),
                        Rule::two => digits.push('2'),
                        Rule::three => digits.push('3'),
                        Rule::four => digits.push('4'),
                        Rule::five => digits.push('5'),
                        Rule::six => digits.push('6'),
                        Rule::seven => digits.push('7'),
                        Rule::eight => digits.push('8'),
                        Rule::nine => digits.push('9'),
                        Rule::decimal => digits.push('.'),
                        _ => {}
                    }
                }
            }
            _ => {}
        }
    }
    match digits.parse() {
        Ok(num) => num,
        Err(_) => 0.0
    }
}
fn extract_item<'a>(parsed: &pest::iterators::Pair<'a,Rule>) -> String {
    let item = extract_item_inner(&mut parsed.clone().into_inner().next().unwrap());//.next().unwrap();
    //match item.as_rule() {
    //    Rule::chimi => "chimi",
    //    Rule::burrito => "burrito",
    //    Rule::can => "can",
    //    Rule::bake => "bake",
    //    Rule::chips => "chips",
    //    Rule::corn => "corn",
    //    Rule::monster => "monster",
    //    Rule::pizza => "pizza",
    //    Rule::twinkie => "twinkie",
    //    Rule::tide => "tide pod",
    //    Rule::chew => "chew",
    //    Rule::muffin => "muffin",
    //    _ => ""
    //}.to_owned()
    item

}
fn extract_item_inner<'a>(parsed: &mut pest::iterators::Pair<'a,Rule>) -> String {
    let item = parsed;
    match item.as_rule() {
        Rule::chimi => "chimi",
        Rule::burrito => "burrito",
        Rule::can => "can",
        Rule::bake => "bake",
        Rule::chips => "chips",
        Rule::corn => "corn",
        Rule::monster => "monster",
        Rule::pizza => "pizza",
        Rule::twinkie => "twinkie",
        Rule::tide => "tide pod",
        Rule::chew => "chew",
        Rule::muffin => "muffin",
        Rule::fish => "fish",
        Rule::nature => "nature",
        _ => ""
    }.to_owned()

}
fn extract_nick<'a>(parsed: &pest::iterators::Pair<'a,Rule>, requestor_nick: &String) -> String {
    let mut nick = String::new();

    for item in parsed.clone().into_inner() {
        match item.as_rule() {
            Rule::nick => {
                for inner_item in item.into_inner() {
                    nick.push_str( match inner_item.as_rule() {
                        Rule::nick_chars => inner_item.clone().into_span().as_str(),
                        _ => ""
                    });
                }
            },
            _ => ()
        }
    }

    if nick.is_empty() {
        nick = requestor_nick.to_string();
    } else {
        nick = nick.chars().rev().collect::<String>()
    }

    nick
}
